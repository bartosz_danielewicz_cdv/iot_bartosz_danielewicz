using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace iot_lab3
{
    public class DatabaseContext
    {
        private readonly string connectionString;

        public DatabaseContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IEnumerable<Person> getPeople()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string getAllQuery = "select * from people";
                
                SqlCommand command = new SqlCommand(getAllQuery, connection);           
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                var people = new List<Person>();
                while (reader.Read())
                {
                    people.Add(new Person
                    {
                        PersonId = Convert.ToInt32(reader["PersonId"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        Phonenumber = reader["Phonenumber"].ToString()
                    });
                }
                reader.Close();

                return people;
            }          
        }

        public Person getPerson(int id)
        { 
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand($"select * from people where PersonId={id}", connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                reader.Read();
                var person = new Person
                {
                    PersonId = Convert.ToInt32(reader["PersonId"]),
                    FirstName = reader["FirstName"].ToString(),
                    LastName = reader["LastName"].ToString(),
                    Phonenumber = reader["Phonenumber"].ToString()
                };
                reader.Close();

                return person;
            }
        }

        public void addPerson(Person personToAdd)
        { 
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(
                    $"insert into people values ('{personToAdd.FirstName}', '{personToAdd.LastName}', '{personToAdd.Phonenumber}')", 
                    connection);
                
                command.ExecuteNonQuery();
            }
        }

        public void deletePerson(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(
                    $"delete from people where personId={id}",
                    connection
                );

                command.ExecuteNonQuery();
            }
        }
    }
}