using System;
using System.IO;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using iot_lab3;

namespace Company.Function
{
    public static class PeopleHttpTrigger
    {
    [FunctionName("getPeople")]
        public static IActionResult getPeople(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PeopleDB");
                log.LogInformation(connectionString);

                var db = new DatabaseContext(connectionString);
                var people = db.getPeople();

                return new JsonResult(people);
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }

        [FunctionName("getPerson")]
        public static IActionResult getPerson(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PeopleDB");
                log.LogInformation(connectionString);

                var db = new DatabaseContext(connectionString);

                int personId = int.Parse(req.Query["personId"]);
                var people = db.getPerson(personId);

                return new JsonResult(people);
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }

        [FunctionName("addPerson")]
        public static IActionResult addPerson(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PeopleDB");
                log.LogInformation(connectionString);

                var db = new DatabaseContext(connectionString);

                var personToAdd = new Person();
                    personToAdd.FirstName = req.Query["FirstName"];
                    personToAdd.LastName = req.Query["LastName"];
                    personToAdd.Phonenumber = req.Query["Phonenumber"];

                db.addPerson(personToAdd);

                return new JsonResult("Person added successfully.");
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }

        [FunctionName("deletePerson")]
        public static IActionResult deletePerson(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PeopleDB");
                log.LogInformation(connectionString);

                var db = new DatabaseContext(connectionString);

                int personId = int.Parse(req.Query["personId"]);
                db.deletePerson(personId);

                return new JsonResult("Person deleted successfully.");
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }
    }
}
