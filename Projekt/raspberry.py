import time
import datetime as datetimelib
import RPi.GPIO as GPIO
import picamera
import os, uuid, sys
from azure.storage.blob import BlockBlobService, PublicAccess

GPIO.setmode(GPIO.BOARD)

ledPinRed = 38
ledPinGreen = 37
ledPinYellow = 35
transistorPin = 33
pirPin = 40

camera = picamera.PiCamera()
camera.vflip = True

GPIO.setup(ledPinRed, GPIO.OUT)
GPIO.setup(ledPinGreen, GPIO.OUT)
GPIO.setup(ledPinYellow, GPIO.OUT)
GPIO.setup(transistorPin, GPIO.OUT)
GPIO.setup(pirPin, GPIO.IN)

def openLockFor(seconds):
    GPIO.output(ledPinRed,GPIO.LOW)
    GPIO.output(ledPinGreen,GPIO.HIGH)
    time.sleep(0.4)

    GPIO.output(transistorPin,GPIO.HIGH)
    time.sleep(seconds)
    GPIO.output(transistorPin,GPIO.LOW)

    GPIO.output(ledPinRed,GPIO.HIGH)
    GPIO.output(ledPinGreen,GPIO.LOW)
    time.sleep(0.4)


def uploadToAzure(dateNameString):
    try:
        # Create the BlockBlockService that is used to call the Blob service for the storage account
        block_blob_service = BlockBlobService(account_name='mykhailodrachonaec6', account_key='IBuxcx4mPxKZ11DJ/yHmdeEh3O4cakG86ellHy1DgH/ce3UGV5t1d5oU+sOlU3RY4+ysJ+lUym+TczY10sEHlQ==')

        # Create a container called 'piCameraBlobs'.
        container_name ='picamerablobs'
        block_blob_service.create_container(container_name)

        # Set the permission so the blobs are public.
        block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)

        # Create a file in Documents to test the upload and download.
        local_path=os.path.abspath(os.path.curdir) + "/shots/"
        local_file_name = dateNameString + ".jpg" #input("Enter file name to upload : ")
        full_path_to_file =os.path.join(local_path, local_file_name)
		
	print(local_path)
	print(local_file_name)
	print(full_path_to_file)

        # Write text to the file.
        #file = open(full_path_to_file,  'w')
        #file.write("Hello, World!")
        #file.close()

        print("Temp file = " + full_path_to_file)
        print("\nUploading to Blob storage as blob " + local_file_name)

        # Upload the created file, use local_file_name for the blob name
        block_blob_service.create_blob_from_path(container_name, local_file_name, full_path_to_file)

        # List the blobs in the container
        print("\nList blobs in the container")
        generator = block_blob_service.list_blobs(container_name)
        for blob in generator:
            print("\t Blob name: " + blob.name)

        # Download the blob(s).
        # Add '_DOWNLOADED' as prefix to '.txt' so you can see both files in Documents.
        full_path_to_file2 = os.path.join(local_path, str.replace(local_file_name, ".jpg","_DOWNLOADED.jpg"))
        print("\nDownloading blob to " + full_path_to_file2)
        block_blob_service.get_blob_to_path(container_name, local_file_name, full_path_to_file2)

        sys.stdout.write("Sample finished running. When you hit <any key>, the sample will be deleted and the sample "
                         "application will exit.")
        sys.stdout.flush()
        input()

        # Clean up resources. This includes the container and the temp files
        block_blob_service.delete_container(container_name)
        os.remove(full_path_to_file)
        os.remove(full_path_to_file2)
    except Exception as e:
        print(e)

def LIGHT(pirPin):
    print("Lights on")
    time.sleep(.2)
    
    dateStringName = datetimelib.datetime.now().strftime("%Y%m%d-%H%M%S")
	

    GPIO.output(ledPinYellow, GPIO.HIGH)
    time.sleep(0.4)

    camera.start_preview()
    time.sleep(5)
    camera.capture('shots/' + dateStringName + '.jpg')
    camera.stop_preview()

    GPIO.output(ledPinYellow, GPIO.LOW)
    time.sleep(0.4)

    openLockFor(5)
    uploadToAzure(dateStringName)
    
    print("Lights off")
    time.sleep(.2)
    
time.sleep(2)
#Set red diode ON/green OFF by default (signal that lock is closed)
GPIO.output(ledPinRed,GPIO.HIGH)
GPIO.output(ledPinGreen,GPIO.LOW)
GPIO.output(ledPinRed,GPIO.LOW)

print("Ready - Kappa123")
    
try:
    GPIO.add_event_detect(pirPin, GPIO.RISING, callback=LIGHT)
    while 1:
        time.sleep(.1)
except KeyboardInterrupt:
    print("Quit")
    GPIO.cleanup()
    
