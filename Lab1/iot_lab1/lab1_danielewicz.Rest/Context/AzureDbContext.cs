using lab1_danielewicz.Rest.Models;
using Microsoft.EntityFrameworkCore;

namespace lab1_danielewicz.Context
{
    public class AzureDbContext : DbContext
    {
        public AzureDbContext(DbContextOptions<AzureDbContext> options) : base(options)
        {
        }

        protected AzureDbContext()
        {
        }

        public DbSet<Person> People { get; set; }
    }
}