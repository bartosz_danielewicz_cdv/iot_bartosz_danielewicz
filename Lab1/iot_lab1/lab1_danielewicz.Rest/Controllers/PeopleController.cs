﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using lab1_danielewicz.Rest.Models;
using lab1_danielewicz.Context;

namespace lab1_danielewicz.Rest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : Controller
    {
        private AzureDbContext dbContext;

        public PeopleController(AzureDbContext context)
        {
            dbContext = context;
        }

        [HttpGet, ActionName("index")]
        public ActionResult<IEnumerable<Person>> Get()
        {
            return dbContext.People.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Person> Get(int id)
        {
            return dbContext.People.Find(id);
        }
        

        [HttpPost, ActionName("add")]
        public ActionResult Post([FromBody] Person newPerson)
        {
            if (ModelState.IsValid)
            {
                dbContext.People.Add(newPerson);
                dbContext.SaveChanges();
            }

            return RedirectToAction("index");
        }

        [HttpPut, ActionName("update")]
        public ActionResult Put([FromBody] Person updatedPerson)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var originalPerson = dbContext.People.Find(updatedPerson.PersonId);

            if (originalPerson != null)
            {
                originalPerson.FirstName = updatedPerson.FirstName;
                originalPerson.LastName = updatedPerson.LastName;

                dbContext.SaveChanges();
            }

            return RedirectToAction("index");
        }

        [HttpDelete("{id}"), ActionName("delete")]
        public ActionResult Delete(int id)
        {
            Person person = dbContext.People.Find(id);

            if (person != null)
            {
                dbContext.People.Remove(person);
                dbContext.SaveChanges();
            }   

            return RedirectToAction("index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbContext.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}